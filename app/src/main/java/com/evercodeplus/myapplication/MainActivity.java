package com.evercodeplus.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.evercodeplus.myapplication.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

//    Button button;
    private ActivityMainBinding mBinding;

    public static final int VALUE = 1;

    public static final int MY_VALUE = 2;

    boolean isFound = false;

    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
//        button = findViewById(R.id.button);
//        button.setOnClickListener(new MyListener());
        mBinding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(v);
            }
        });
        mBinding.text1.setText("Text");
        mBinding.text5.setText("Hello");

        MyFirstClass myFirstClass = new MyFirstClass("abc");
        myFirstClass.setId(1);
        myFirstClass.setName("Abc");
        myFirstClass.setMarks(12.3);
//        myFirstClass.setName("test");
        Log.d(TAG, "start: "+myFirstClass.getMarks());
        int a = 50;
        Log.d(TAG, "start: "+a);
        test( myFirstClass, a);
        Log.d(TAG, "end: "+a);
        Log.d(TAG, "end: "+myFirstClass.getMarks());

    }

    void foo() {
        Log.d(TAG, "foo: 1");
        foo1();
    }

    void foo1() {
        Log.d(TAG, "foo: 2");
        foo2();
    }

    void foo2() {
        Log.d(TAG, "foo: 3");
    }

    void test(MyFirstClass myFirstClass, int a) {
//        Log.d(TAG, "test: "+myFirstClass.getName());
//        Log.d(TAG, "test: "+myFirstClass.getId());
//        Log.d(TAG, "test: "+myFirstClass.getMarks());
        myFirstClass.setMarks(200);
        a = 10;
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "click: Here");
    }

    public void click(View view) {
        Log.d(TAG, "click: Here");
    }

    public void async(View view) {
        new MyTask().execute();
        new MyTask1().execute();
        new MyTask2().execute();
    }

    public void call(View view) {
        foo();
    }


    class MyListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Log.d(TAG, "click: Here");
        }
    }

    class MyTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            for(int i = 0;i<100000;i++) {
                Log.d(TAG, "click: Here");
            }
            return null;
        }
    }

    class MyTask1 extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            for(int i = 0;i<100000;i++) {
                Log.d(TAG, "Hello");
            }
            return null;
        }
    }

    class MyTask2 extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            for(int i = 0;i<100000;i++) {
                Log.d(TAG, "Bye");
            }
            return null;
        }
    }

}
