package com.evercodeplus.myapplication;

public class MyFirstClass {
    private int id = 10;
    private String name;
    private double marks = 1.2;
    private boolean isFound = true;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMarks() {
        return marks;
    }

    public void setMarks(double marks) {
        this.marks = marks;
    }

    public MyFirstClass(String name) {
        this.name = name;
    }
}
